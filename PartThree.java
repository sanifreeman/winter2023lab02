import java.util.Scanner;
public class PartThree{
    public static void main(String[]args){
        Scanner keyboard= new Scanner(System.in);
        int value1=keyboard.nextInt();
        int value2=keyboard.nextInt();
        //this is for the static methods
       int caladd= Calculator.add(value1,value2);
       int calsub= Calculator.subtract(value1,value2);
      //now for non static method 
       Calculator sc= new Calculator();
       int calmult=sc.multiply(value1,value2);
       int caldiv=sc.divide(value1,value2);
       System.out.println(caldiv);
       System.out.println(calmult);
       System.out.println(caladd);
       System.out.println(calsub);
        

    }
}