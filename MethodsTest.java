public class MethodsTest{
  public static void main(String[]args){
   int x =5; 
   System.out.println(x);
   methodNoInputReturn();
   System.out.println(x);
   System.out.println(x);
   methodOneInputNoReturn(x+10);
   System.out.println(x);
   methodTwoInputNoReturn( 5,  6.7);
   int c=methodNoInputReturnInt();
   System.out.println(c);
   double res=sumSquareRoot(9, 5);
   System.out.println(res);
   String s1="java";
   String s2="programming";
   
   System.out.println(s1.length());
   System.out.println(s2.length());

   int theansw=SecondClass.addOne(50);
   System.out.println(theansw);
   
   SecondClass sc= new SecondClass();
   int answ=sc.addTwo(50);
   System.out.println(answ);
  }
  public static void methodNoInputReturn(){
    System.out.println("in a method that takes no input and returns nothing");
    int x =20;
    System.out.println(x);
  }
  public static void methodOneInputNoReturn(int pizza){
    pizza= pizza-5;
    System.out.println("inside the method one input no return");
    System.out.println(pizza);
  }
  public static void methodTwoInputNoReturn(int valuei, double valued){
    System.out.println(valuei);
    System.out.println(valued);
  }
  public static int methodNoInputReturnInt(){
    int z=5;
    return z;
  }
  public static double sumSquareRoot(int first, int second){
double thirdV=first+second;
double answ=Math.sqrt(thirdV);
return answ;
  }
}